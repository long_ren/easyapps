require.config({
    baseUrl: "../../js",
})


require(["EasyApp"], function($) {
    var ContentCtl = avalon.define({
        $id: "ContentCtl",
        App:{},
        vo:{},
        
        goBack:function(){
            // api.closeWin({
            //     animation:{
            //         subType:"from_left", 
            //     }
            // });
            api.openWin({name:'slidLayout'})
        },
        run:function(){
            ContentCtl.App = $.App();
            var id = $.getUrlParam('id');
            $.getContent(ContentCtl.App.id,id,function(req){
                ContentCtl.vo = req;
            })
        }
    })

    $(function(){
        ContentCtl.run();
        avalon.scan();
        api.addEventListener({name: 'keyback'}, function(ret, err){
            // api.openWin({name:'slidLayout',subType:"from_right",});
            api.closeWin({
                animation:{
                    type:"push",
                    subType:"from_left", 
                }
            });
        });
    })


})