$(document).ajaxStart(function(){
    NProgress.start();
}).ajaxComplete(function(){
    NProgress.done();
}).ajaxError(function(){
    $.bootstrapGrowl('未知的系统错误', {
        type: 'danger',
        align: 'center',
    });
})


$(function(){
    var n = $("#sidebar").hasClass("menu-compact");
    $(".sidebar-menu").on("click",
    function(t) {
        var i = $(t.target).closest("a"),
        u,
        r,
        f;
        if (i && i.length != 0) {
            if (!i.hasClass("menu-dropdown")) return n && i.get(0).parentNode.parentNode == this && (u = i.find(".menu-text").get(0), t.target != u && !$.contains(u, t.target)) ? !1 : void 0;
            if (r = i.next().get(0), !$(r).is(":visible")) {
                if (f = $(r.parentNode).closest("ul"), n && f.hasClass("sidebar-menu")) return;
                f.find("> .open > .submenu").each(function() {
                    this == r || $(this.parentNode).hasClass("active") || $(this).slideUp(200).parent().removeClass("open")
                })
            }
            return n && $(r.parentNode.parentNode).hasClass("sidebar-menu") ? !1 : ($(r).slideToggle(200).parent().toggleClass("open"), !1)
        }
    })



    $('select[data-value]').each(function(){
        var v = $(this).attr('data-value');
        if (v) 
            $(this).val(v);
    })

    $("form[valid]").validVal();

    $('select[data-page]').on('change',function(){
        var url = window.location.href;
        var page = $(this).val();
        url = U('page',page,url)
        NProgress.start();
        window.location.href = url;
        //
    })

    $('select[data-jump]').on('change',function(){
        var url = window.location.href;
        var f = $(this).data('jump');
        var v = $(this).val();
        var mp = {};
        mp[f] = v;
        url = urlParas(url).set(mp);
        NProgress.start();
        window.location.href = url;
        //
    })

    // $(document).on('click','[ajax-submit]',function(e){
    //     e.preventDefault();
    //     var $form = $(this).parents('form');
    //     var url = $form.attr('action');
    //     var form_data = $form.triggerHandler('submitForm');
    //     if ( form_data || typeof form_data == "undefined") {
    //         var post_data = $form.serialize();
    //         $.post(url,post_data,function(req){
    //             $.bootstrapGrowl(req['info'], {
    //                 type: 'danger',
    //                 align: 'center',
    //             });
    //             if (req['status'] == '1') {
    //                 window.location.reload();
    //             };
    //         })
    //     }
    // })
    
    $(document).on('click','a[data-ajax]',function(event){
        event.preventDefault();
        var url = $(this).attr('href');
        $.get(url,function(req){
            $.bootstrapGrowl(req['info'], {
                type: 'danger',
                align: 'center',
            });
        })
    })

    $(document).on('click','[ajax-dialog]',function(event){
        event.preventDefault();
        var size = $(this).attr('dialog-size');
        var url = $(this).attr('href');
        var title = $(this).attr('dialog-title');
        BootstrapDialog.show({
            message: $('<div></div>').load(url),
            size:size,
            title:title,
        });
    })

    $(document).on('click','[ajax-submit]',function(e){
        $.bootstrapGrowl('数据提交中...', {
            type: 'info',
            align: 'center',
        });
        e.preventDefault();
        var $form = $(this).parents('form');
        var url = $form.attr('action');
        var form_data = $form.triggerHandler('submitForm');
        if ( form_data || typeof form_data == "undefined") {
            var post_data = $form.serialize();
            $.post(url,post_data,function(req){
                $.bootstrapGrowl(req['info'], {
                    type: 'danger',
                    align: 'center',
                });
                if (req['status'] == '1') {
                    if (req.url) {
                        window.location.href = req.url;
                    }else{
                        window.location.reload();
                    }
                };
            })
        }
    })

    $('.fileinput').on('click',function(event){
        event.preventDefault();
        var fname = $(this).data('name');
        BootstrapDialog.show({
            message: $('<div></div>').load('user.php?c=FileManger&fname='+fname),
            title:'文件管理',
            size:'size-wide',
        });
    })

    $('[ajax-confirmation]').confirmation({
        onConfirm:function(event, element){
            event.preventDefault();
            var url =  $(element).attr('href');
            $.get(url,function(){
                window.location.reload();
            });
        }
    });
})



function U(arg,arg_val,url){ 
    if (!url) url = window.location.href;
    var pattern=arg+'=([^&]*)'; 
    var replaceText=arg+'='+arg_val; 
    if(url.match(pattern)){ 
        var tmp='/('+ arg+'=)([^&]*)/gi'; 
        tmp=url.replace(eval(tmp),replaceText); 
        return tmp; 
    }else{ 
        if(url.match('[\?]')){ 
            return url+'&'+replaceText; 
        }else{ 
            return url+'?'+replaceText; 
        } 
    } 
    return url+'\n'+arg+'\n'+arg_val; 
}