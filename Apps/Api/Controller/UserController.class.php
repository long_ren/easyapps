<?php
namespace Api\Controller;
use Think\Controller;
class UserController extends CommonController {
    public function index(){
        $Page = D('Page');
        $map['apid'] = $this->apid;
        $data = $Page->getPage($map)->select();
        $this->ajaxReturn($data,$this->format);
    }

    public function ckLogin($password='',$account = '')
    {
        if (!$password || !$account) {
            $data['ret'] = '0';
            $data['msg'] = '账号或密码不能为空';
            $this->ajaxReturn($data,$this->format);
            return;
        }

        $map['appid'] = $this->apid;
        $map['account'] = $account;
        $map['password'] = $password;
        $User = M('User');
        $user = $User->where($map)->find();
        if (!$user) {
            $data['ret'] = '0';
            $data['msg'] = '用户名或者密码错误';
            $this->ajaxReturn($data,$this->format);
            return;
        }

        if ($user) {
            $data['ret'] = '1';
            $data['msg'] = '登陆成功';
            $data['User'] = $user;
            $this->ajaxReturn($data,$this->format);
            return;
        }
    }

    public function ckReg($repwd = '',$password ='',$account = '',$mobile = '')
    {

        if (!$repwd || !$password || !$account) {
            $data['ret'] = '0';
            $data['msg'] = '账号或密码不能为空';
            $this->ajaxReturn($data,$this->format);
            return;
        }
        if (strlen($password) < 5) {
            $data['ret'] = '0';
            $data['msg'] = '密码长度不能小于5位';
            $this->ajaxReturn($data,$this->format);
            return;
        }
        if ($repwd != $password) {
            $data['ret'] = '0';
            $data['msg'] = '两次输入的密码不匹配';
            $this->ajaxReturn($data,$this->format);
            return;
        }
        $map['appid'] = $this->apid;
        $map['account'] = $account;
        $User = M('User');
        $user = $User->where($map)->find();
        if ($user) {
            $data['ret'] = '0';
            $data['msg'] = '该用户名已经存在。';
            $this->ajaxReturn($data,$this->format);
            return;
        }

        $d['password'] = md5($password);
        $d['account'] = $account;
        $d['mobile'] = $mobile;
        $d['appid'] = $this->apid;
        $ret = $User->add($d);
        if ($ret !== FALSE) {
            $data['ret'] = '1';
            $data['msg'] = '注册成功，请登陆。';
            $this->ajaxReturn($data,$this->format);
            return;
        }else {
            $data['ret'] = '0';
            $data['msg'] = '注册失败，系统错误。';
            $this->ajaxReturn($data,$this->format);
            return;
        }

    }
}